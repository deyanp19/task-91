import EventEmitter from "eventemitter3";
import Beat from "./Beat";

  

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }


  constructor(count) {
    super();
    this.count=0;
    this._beat = new Beat();
    this._beat.on(Beat.events.BIT, () => this._create())
    this.emit(Application.events.READY);
    this._create = (count) => {

      const lyrics = ["Ah", "ha", "ha", "ha", "stayin' alive", "stayin' alive"];

      const message = document.createElement("div");
      message.classList.add("message");
      message.innerText = lyrics[this.count]

      document.querySelector(".main").appendChild(message);
      this.count++;
      if (this.count==lyrics.length) {
       this.removeListener('bit',this._create)
      }
    }
  }

 
}